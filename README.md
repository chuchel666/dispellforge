I can't run the `instawow` due to different python version on my system and have been too tired to install the proper one, so I decided to develop my personal manager.

Install all dependencies whatever way you want and have fun.

Tested on linux system, I don't see any reason to use it on windows, but you can try.

Licensed under [![WTFPL Logo](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/)
