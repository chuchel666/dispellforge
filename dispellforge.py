import requests
import urllib.request
import os
import yaml
import re
import sys
import zipfile

class Api:
    def __init__(self, game_version):
        self.base_url = 'https://addons-ecs.forgesvc.net/'
        self.api_url = {'info' : "api/v2/addon/{}", "download_url" : "api/v2/addon/{}/file/{}/download-url", "search" : "api/v2/addon/search"}
        self.user_agent = 'curl'
        self.game_version = game_version

    def get_addon_info(self, addon_id):
        url = self.base_url+self.api_url['info'].format(addon_id)
        headers = {'User-Agent' : self.user_agent}
        r = requests.get(url=url, headers=headers)
        data = r.json()

        return data

    def get_download_url(self, addon_id, file_id):
        url = self.base_url+self.api_url['download_url'].format(addon_id, file_id)
        headers = {'User-Agent' : self.user_agent}
        r = requests.get(url=url, headers=headers)

        data = r.text

        return data

    def search(self, name):
        url = self.base_url+self.api_url['search']
        headers = {'User-Agent' : self.user_agent}
        params = {'categoryID' : 0, 'gameId' : 1, 'gameVersion' : self.game_version, 'index' : 0, 'pageSize' : 25, 'searchFilter' : name, 'sectionId' : 0, 'sort' : 0}
        r = requests.get(url=url, headers=headers, params=params)

        data = r.json()

        return data

class DispellForge:
    def __init__(self, flavor):
        self.download_dir = './downloads'

        if not os.path.exists('config.yaml'):
            print("Seems like you don't have a configuration file yet. File will be created, but you have to set up all necessary fields and relaunch the application; now exiting.")
            with open('config.yaml', 'w') as config_file:
                config_file.write("addons_dir: \ngame_version: \n")

            sys.exit(1)

        with open(r'config.yaml') as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)
            self.addons_dir = config['addons_dir'].format(flavor)
            self.game_version = config['game_version']

        self.api = Api(self.game_version)

    def get_latest_info(self, addon_id):
        info = self.api.get_addon_info(addon_id)
        print(info["gameVersionLatestFiles"][0])

    def download(self, addon_id):
        info = self.api.get_addon_info(addon_id)

        file_id = None
        file_name = None

        for ver in info["gameVersionLatestFiles"]:
            if ver["gameVersion"] == self.game_version:
                file_id = ver['projectFileId']
                file_name = ver['projectFileName']
                break
        if file_id is None:
            raise ValueError("Bad game version or no addon files for game version")

        url = self.api.get_download_url(addon_id, file_id)

        urllib.request.urlretrieve(url, os.path.join(self.download_dir, file_name))

        return os.path.join(self.download_dir, file_name)

    def get_list(self):
        files = os.listdir(self.addons_dir)
        addons = []

        for addon in files:
            #print('Processing {}'.format(addon))
            if not os.path.exists(os.path.join(self.addons_dir, addon, addon+'.toc')):
                raise ValueError("Cannot retrieve addon info for {}".format(addon))
            with open(os.path.join(self.addons_dir, addon, addon+'.toc'), 'r') as desc:
                    head = desc.read()
                    m = re.search("Version:\s+(.+)", head)
                    if m:
                        version = m.group(1)
                        obj = {"name" : addon, "version" : version}
                        m = re.search("X-Curse-Project-ID:\s+(.+)", head)
                        if m:
                            addon_id = m.group(1)
                            obj['id'] = addon_id
                        else:
                            obj['id'] = 'UNMANAGED'
                        #print("Version of {} is {}".format(addon, version))
                        addons.append(obj)

        return addons

    def update(self, addon):
        addons = self.get_list()
        try:
            addon = int(addon)
        except ValueError as e:
            pass
        if type(addon) == int:
            addon_id = addons[addon]['id']
            if addon_id == 'UNMANAGED':
                print("Addon {} version is undefined, perhaps this addon was not been installed via CurseForge API. Do you want to search for this addon in CurseForge database?".format(addons[addon]['name']))
                a = ''
                while(a != 'y' and a != 'n'):
                    a = input("y(es)/n(o)")
                if a == 'n':
                    print('Ok, aborting then')
                    return
                elif a == 'y':
                    print("Searching for {}...".format(addons[addon]['name']))

                    found = self.print_search(addons[addon]['name'])

                    if len(found) == 0:
                        print('No addon found; aborting.')
                        return

                    update_id = ''
                    while update_id not in [str(x['id']) for x in found]:
                        update_id = input('Choose addon ID: ')

                    path = self.download(update_id)

                    with zipfile.ZipFile(path, 'r') as zip_ref:
                        zip_ref.extractall(self.addons_dir)

                    print('Successfully updated')

    def print_list(self):
        print("# Name Version ID")
        addons = self.get_list()
        count = 0
        for addon in addons:
            print("{} {} {} {}".format(count, addon["name"], addon["version"], addon["id"]))
            count += 1

    def print_search(self, name):
        addons = self.search(name)

        print("ID NAME")
        for addon in addons:
            print("{} {}".format(addon['id'], addon['name']))

        return addons

    def search(self, name):
        raw = self.api.search(name)
        result = [{"id":a["id"], "name":a["name"]} for a in raw]

        return result

    def install(self, name):
        print("Searching for addon {}...".format(name))
        addons = self.print_search(name)

        id_input = ''
        while id_input not in [str(x['id']) for x in addons]:
            id_input = input('Choose ID to install: ')
        print("Downloading...")
        path = self.download(id_input)
        print("Unzipping...")
        with zipfile.ZipFile(path, 'r') as zip_ref:
            zip_ref.extractall(self.addons_dir)

        print('Successfully installed')

def main():
    if len(sys.argv) < 2:
        print("Usage:\nlist print list of addons\nsearch [name] search specified addon for specified game version")
        return

    args = sys.argv[1:]

    forge = DispellForge('retail')

    if args[0] == 'list':
        forge.print_list()
    elif args[0] == 'search':
        if len(args) < 2:
            print("Usage: search [name]")
            return
        print(forge.search(args[1]))
    elif args[0] == 'update':
        if len(args) >= 2:
            for i in range(1, len(args)):
                forge.update(args[i])
    elif args[0] == 'install':
        if len(args) < 2:
            print('Usage: install [name]')
            return
        else:
            forge.install(' '.join(args[1:]))

if __name__ == '__main__':
    main()

